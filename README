gpt_builder
===========

Tool for generating the partition table ('gpt.img' binary). Generated image is
further written to 'gpt' partition via fastboot tool, which changes eMMC's
partition table.

Usage
-----

WARNING! Flashing 'gpt.img' with incorrect partition table can brick your board!

Prepare your layout file. It's recommended to copy over the 'gpt_layout' file
and modify it, and only change partitions between 'super' and 'userdata';
otherwise the boot sequence can be disrupted, which in turn can lead to a
bricked board.

Format is as follows: each line describes one partition and consists of four
columns:
  1. LUN:
     0 - User partition
     1 - Boot partition A
     2 - Boot partition B
  2. FILESYS:
     0 - OS can't see that
     1 - OS can see that and we assume BOOTLOADER writes something continuously
     2 - OS can see that and we assume BOOTLOADER gets EXT4 filesystem image
         transformed into sparse format and writes it
     3 - OS can see that and we assume BOOTLOADER gets F2FS filesystem image
         transformed into sparse format and writes it
  3. BLKNUM: partition size in LBAs (for eMMC, 1 LBA = 512 bytes)
  4. NAME: partition name

For example see 'gpt_layout' file.

Once the layout file is prepared, run the 'gpt_builder' tool:

    $ ./gpt_builder_ver_0_2 -d mmc -i <your_layout_file> -o <image_file_name>

To apply the generated image, flash it to 'gpt' partition using fastboot.
